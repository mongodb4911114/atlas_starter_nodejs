const MongoClient = require('mongodb').MongoClient;
const {User} = require('./models/schemas');
console.log('🚀 ~ User:', User);

// const uri = `mongodb+srv://eli:eli@clustereli.1mm4wrw.mongodb.net/${db}?retryWrites=true&w=majority&appName=ClusterEli`;
const uri = `mongodb+srv://eli:eli@clustereli.1mm4wrw.mongodb.net/?retryWrites=true&w=majority&appName=ClusterEli`;
// const uri = `mongodb://eli:eli@clustereli.1mm4wrw.mongodb.net:27017/?retryWrites=true&w=majority&appName=ClusterEli`;
//
console.log('🚀 ~ uri : ', uri);
// console.log('🚀 ~ uriMflix:', uriMflix);

// Create a Mongoclient with a Mongoclient Options object to set the Stable API version

const options = {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	// serverApi: {
	// 	version: '1',
	// 	strict: true,
	// 	deprecationErrors: true,
	// },
};
const client = new MongoClient(uri, options);

const createDB = (db, collec) => {
	client
		.connect()
		.then(() => {
			console.log('Le client est connecté avec succès.');

			return client
				.db(db)
				.createCollection(collec)
				.then(() => {
					console.log(
						`La collection ${collec} a été créée avec succès dans la base de données ${db}.`
					);
				})
				.catch((error) => {
					console.error(
						`Une erreur est survenue lors de la création de la collection ${collec} :`,
						error
					);
				});
		})

		.catch((error) => {
			console.error('Une erreur est survenue:', error);
		})
		.finally(() => {
			client
				.close()
				.then(() => {
					console.log('Le client est fermé avec succès.');
				})
				.catch((error) => {
					console.error(
						'Une erreur est survenue lors de la fermeture du client:',
						error
					);
				});
		});
};
//## ci dessous findExistUser AVEC connexion prélable -- DESACTIVE POUR CAUSE DE CONNEXION DECONNEXION
// const findExistUser = (oneUser, db, collec) => {
// 	const {email, user} = oneUser; // Identifiant de l'utilisateur à rechercher
// 	const existUser = null; // Variable qui stockera l'utilisateur trouvé

// 	return client
// 		.connect()
// 		.then(() => {
// 			console.log('CONNECTÉ ---> RECHERCHE EN COOURS .....');

// 			return client
// 				.db(db)
// 				.collection(collec)
// 				.findOne({
// 					$or: [
// 						{email: email}, // Recherche par e-mail
// 						{name: user}, // Recherche par nom d'utilisateur
// 					],
// 				})
// 				.then((existUser) => {
// 					if (existUser) {
// 						console.log(
// 							`Utilisateur trouvé existant (email ou pseudo existant)`,
// 							existUser
// 						);
// 						return existUser; // Retourne l'utilisateur trouvé
// 					} else {
// 						console.log(
// 							`Aucun utilisateur trouvé avec l'e-mail ou le nom d'utilisateur ${email} /  ${user}  .`
// 						);
// 						return null; // Aucun utilisateur trouvé
// 					}
// 				})
// 				.catch((error) => {
// 					console.error(
// 						"Erreur lors de la recherche de l'utilisateur :",
// 						error
// 					);
// 					throw error; // Propagation de l'erreur
// 				});
// 		})
// 		.catch((error) => {
// 			console.error(
// 				'Une erreur est survenue lors de la connexion :',
// 				error
// 			);
// 			throw error; // Propagation de l'erreur
// 		})
// 		// .finally(() => {
// 		// 	return client
// 		// 		.close()
// 		// 		.then(() => {
// 		// 			console.log('CONNEXION FERMEE ---> Ok ! ');
// 		// 		})
// 		// 		.catch((error) => {
// 		// 			console.error(
// 		// 				'Une erreur est survenue lors de la fermeture du client:',
// 		// 				error
// 		// 			);
// 		// 		});
// 		// });
// };
//

const findExistUser = (oneUser, db, collec) => {
	const {email, user} = oneUser; // Identifiant de l'utilisateur à rechercher
	const existUser = null; // Variable qui stockera l'utilisateur trouvé

	return client
		.db(db)
		.collection(collec)
		.findOne({
			$or: [
				{email: email}, // Recherche par e-mail
				{name: user}, // Recherche par nom d'utilisateur
			],
		})
		.then((existUser) => {
			if (existUser) {
				console.log(
					`Utilisateur trouvé existant (email ou pseudo existant)`,
					existUser
				);
				return existUser; // Retourne l'utilisateur trouvé
			} else {
				console.log(
					`Aucun utilisateur trouvé avec l'e-mail ou le nom d'utilisateur ${email} /  ${user}  .`
				);
				return null; // Aucun utilisateur trouvé
			}
		})
		.catch((error) => {
			console.error(
				"Erreur lors de la recherche de l'utilisateur :",
				error
			);
			throw error; // Propagation de l'erreur
		})
		.catch((error) => {
			console.error(
				'Une erreur est survenue lors de la connexion :',
				error
			);
			throw error; // Propagation de l'erreur
		});
	// .finally(() => {
	// 	return client
	// 		.close()
	// 		.then(() => {
	// 			console.log('CONNEXION FERMEE ---> Ok ! ');
	// 		})
	// 		.catch((error) => {
	// 			console.error(
	// 				'Une erreur est survenue lors de la fermeture du client:',
	// 				error
	// 			);
	// 		});
	// });
};

// const countDoc = (db, collec) => {
// 	return client
// 		.connect()
// 		.then(() => {
// 			console.log('CONNECTÉ : RECHERCHE D4UN EXISTANT ....');

// 			return client
// 				.db(db)
// 				.collection(collec)
// 				.countDocuments()
// 				.then((count) => {
// 					console.log(
// 						`Il y a ${count} DOCUMENTS dans la collection ${collec}.`
// 					);
// 					return count; // Retourne le nombre de documents
// 				})
// 				.catch((error) => {
// 					console.error(
// 						'ERREUR lors du COMPTE des documents :',
// 						error
// 					);
// 					throw error; // Propagation de l'erreur
// 				});
// 		})
// 		.catch((error) => {
// 			console.error(
// 				'Une erreur est survenue lors de la connexion :',
// 				error
// 			);
// 			throw error; // Propagation de l'erreur
// 		});
// };

const countDoc = (db, collec) => {
	return client
		.connect()
		.then(() => {
			console.log('CONNECTÉ : RECHERCHE D UN EXISTANT ....');

			return client
				.db(db)
				.collection(collec)
				.countDocuments()
				.then((count) => {
					console.log(
						`Il y a ${count} DOCUMENTS dans la collection ${collec}.`
					);
					return count; // Retourne le nombre de documents
				})
				.catch((error) => {
					console.error(
						'ERREUR lors du COMPTE des documents :',
						error
					);
					throw error; // Propagation de l'erreur
				})
				.finally(() => {
					return client
						.close()
						.then(() => {
							console.log('CONNEXION FERMÉE avec succès.');
						})
						.catch((error) => {
							console.error(
								'Une erreur est survenue lors de la fermeture de la connexion :',
								error
							);
						});
				});
		})
		.catch((error) => {
			console.error(
				'Une erreur est survenue lors de la connexion :',
				error
			);
			throw error; // Propagation de l'erreur
		});
};

const connectDB = (db, collec) => {
	client
		.connect()
		.then(() => {
			console.log(`CONNECTÉ à ${db} sur la collection ${collec}`);
		})
		.catch((error) => {
			console.error('Une erreur est survenue:', error);
		});
	// .finally(() => {
	// 	client
	// 		.close()
	// 		.then(() => {
	// 			console.log('CONNEXION FERMEE ---> Ok ! ');
	// 		})
	// 		.catch((error) => {
	// 			console.error(
	// 				'Une erreur est survenue lors de la fermeture du client:',
	// 				error
	// 			);
	// 		});
	// });
};

const createUSER = (oneUser, db, collec) => {
	client
		.connect()
		.then(() => {
			console.log(`CONNECTÉ à ${db} sur la collection ${collec}`);

			return client
				.db(db)
				.collection(collec)
				.insertOne(oneUser)
				.then(() => {
					console.log(
						`User :  ${oneUser}  créée avec succès dans la base de données ${db}.`
					);
				})
				.catch((error) => {
					console.error(
						`Erreur  lors de la création du User  :`,
						error
					);
				});
		})

		.catch((error) => {
			console.error('Une erreur est survenue:', error);
		});
	// .finally(() => {
	// 	client
	// 		.close()
	// 		.then(() => {
	// 			console.log('Le client est fermé avec succès.');
	// 		})
	// 		.catch((error) => {
	// 			console.error(
	// 				'Une erreur est survenue lors de la fermeture du client:',
	// 				error
	// 			);
	// 		});
	// });
};

module.exports = {
	createDB,
	createUSER,
	findExistUser,
	countDoc,
	connectDB,
};
