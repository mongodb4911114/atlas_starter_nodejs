// const {connecToDB, getDB} = require('./dbConnectCluster');
const express = require('express');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const {
	createDB,
	createUSER,
	findExistUser,
	connectDB,
	countDoc,
} = require('./actionDB');
const {User} = require('./models/schemas');
const myIP = require('./getPublicIP');

//
const app = express();
//
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.set('view engine', 'ejs');
app.use(express.static('public'));

console.log('🚀 ~ User SCHEMA:', User);

//## -------------------------------------------------------------------------

app.route('/')
	.get((req, res) => {
		// res.json('ROUTE GET RACINE PUR ACCUEIL');
		connectDB('membres', 'normal');
		res.render('index', {});
	})
	.post((req, res) => {
		res.json('Route POST pour la racine');
	});

app.get('/publicip', (req, res) => {
	// Utiliser la promesse myIP pour obtenir l'adresse IP publique
	myIP.then((ip) => {
		// Renvoyer l'adresse IP obtenue
		// res.send(`Adresse IP publique : ${ip}`);
		res.status(200).render('public_ip', {ip});
	}).catch((error) => {
		console.error(error);
		res.status(500).render('error_ip', {error});
	});
});

app.route('/subdb')
	.get((req, res) => {
		//
		res.render('create_db_colec', {db: 'db', collec: 'collec'});
		//
		//
	})
	.post((req, res) => {
		const {db, collec} = req.body;

		console.log('🚀 ~ .post ~ db / collec : ', db, '/', collec);

		createDB(db, collec);

		countDoc(db, collec)
			.then((count) => {
				// Utiliser le nombre de documents ici
			})
			.catch((error) => {
				// Gérer les erreurs ici
			});
		// res.status(200).send(
		// 	`BASE et COLLEC crées: ${JSON.stringify({db, collec})}`
		// );
		res.status(200);
		//

		//
	});

app.route('/create-user')
	.get((req, res) => {
		//
		res.render('create_user', {
			oneUser: {
				user: 'user',
				email: 'email',
				pwd: '<PASSWORD>',
				role: 'role',
			},
		});
		//
		//
	})
	.post((req, res) => {
		const {user, email, pwd, role} = req.body;

		const oneUser = new User({
			name: user,
			email: email,
			pwd: pwd,
			role: role,
		});
		console.log(' 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 ');

		console.log('🚀 ~ .post ~ role:', role);
		console.log('🚀 ~ .post ~ pwd:', pwd);
		console.log('🚀 ~ .post ~ email:', email);
		console.log('🚀 ~ .post ~ name:', user);

		if (!user || !email || !pwd || !role) {
			return res
				.status(400)
				.send({error: 'Tous les champs sont obligatoires'});
		}

		// const isit = findExistUser(oneUser, 'membres', 'normal');
		// console.log('🚀 ~ .post ~ isit:', isit);

		createUSER(oneUser, 'membres', 'normal');

		console.log('🚀 ~ - ONE USER  : ', oneUser);
		// res.render('popup_user', {oneUser});

		countDoc('membres', 'normal')
			.then((count) => {
				// Utiliser le nombre de documents ici
			})
			.catch((error) => {
				// Gérer les erreurs ici
			});

		res.status(200).send(
			`Utilisateur créé: ${JSON.stringify({user, email, pwd, role})}`
		);

		//
		//
		//
		//
	});

//
//
//
//

app.route('/search')
	.get((req, res) => {
		//
		res.render('search', {});
		//
		//
	})
	.post((req, res) => {
		const {user, email} = req.body;
		console.log('🚀 ~ .post ~ email:', email, '🚀 ~ .post ~ user:', user);
		const isit = findExistUser(oneUser, 'membres', 'normal');
		if (isit) {
			res.render('popup', {isit});
		} else {
			res.render('popup', {isit});
		}

		// res.render('popup', {db, collec});
		//

		//
	});

//##
app.listen(3000, () => {
	console.log('Serveur OK sur : http://localhost:3000 ');
});
