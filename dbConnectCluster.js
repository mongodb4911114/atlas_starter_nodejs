const {MongoClient, ServerApiVersion} = require('mongodb');
const URI =
	'mongodb+srv://eli:eli@clustereli.1mm4wrw.mongodb.net/?retryWrites=true&w=majority&appName=ClusterEli';

let dbConnection;
module.exports = {
	// "url": "mongodb://localhost:27017/mydb"
	connecToDB: (callBack) => {
		MongoClient.connect(URI, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		})
			.then((client) => {
				dbConnection = client.db();
				return callBack();
			})
			.catch((err) => {
				console.log(err);
				return callBack();
			});
	},
	getDB: () => dbConnection,
};
