const MongoClient = require('mongodb/lib/mongo_client');
const clientConnect = new MongoClient(uri, options);

const addCollec = (db, collec) => {
	return db.createCollection(collec).then(() => {
		console.log(
			`La collection "${collec}" a été créée avec succès dans la base de données "${db}".`
		);
	});
};

const addDoc = (db, collec, doc) => {
	return db
		.collection(collec)
		.insertOne(doc)
		.then((result) => {
			console.log(
				`Un document a été inséré avec succès dans la collection "${collec}" de la base de données "${db}".`
			);
			return result;
		});
};

// const createDataBaseAsynChrone = async () => {
// 	try {
// 		await client.connect();
// 		console.log('Le client est connecté avec succès.');
// 	} catch (error) {
// 		console.error('Une erreur est survenue lors de la connexion:', error);
// 	}
// };

const createDB = (client) => {
	return new Promise(async (resolve, reject) => {
		try {
			await client.connect();
			console.log('Le client est connecté avec succès.');
			resolve();
		} catch (error) {
			console.error(
				'Une erreur est survenue lors de la connexion:',
				error
			);
			reject(error);
		}
	});
};

const createDataBase = (client, dataBase) => {
	return client
		.connect()
		.then(() => {
			return client
				.db(dataBase)
				.then(() => {
					console.log(
						`La base de données ${dataBase} a été créée avec succès.`
					);
				})
				.catch((error) => {
					console.error(
						`Une erreur est survenue lors de la création de la base de données ${dataBase} :`,
						error
					);
				});
		})
		.catch((error) => {
			console.error(
				'Une erreur est survenue lors de la connexion :',
				error
			);
		});
};

//

const connectDataBase = (client) => {
	return client
		.connect()
		.then(() => {
			console.log('Le client est connecté avec succès.');
		})
		.catch((error) => {
			console.error(
				'Une erreur est survenue lors de la connexion :',
				error
			);
		});
};

module.exports = {
	addCollec,
	addDoc,
	createDataBaseAsynChrone,
	createDB,
	createDataBase,
	connectDataBase,
};
