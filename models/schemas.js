const mongoose = require('mongoose');

const valEmailRegex = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/gim;

const valPWD =
	/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&_])[A-Za-z\d$@$!%*?&_]{8,24}$/;

const valPseudo = /^[a-zA-Z]+$/;
// Définir le schéma de l'utilisateur
const userSchemaBASE = new mongoose.Schema({
	// pas utilisé now
	name: {
		type: String,
		unique: true, // Assure l'unicité du nom
		required: true, // Le nom est requis
		minlength: 3, // Le nom doit contenir au moins 3 caractères
		maxlength: 24, // Le nom doit contenir au plus 20 caractères
		trim: true, // Le nom ne doit pas contenir d'espaces en début ou fin
		lowercase: true, // Le nom ne doit pas contenir de majuscules
		validate: {
			validator: function (pseudo) {
				// Expression régulière pour valider le nom
				return valPseudo.test(pseudo);
			},
			message: "Le nom n'est pas valide",
		},
	},
	email: {
		type: String,
		unique: true, // Assure l'unicité de l'email
		required: true, // L'email est requis
		validate: {
			validator: function (email) {
				// Expression régulière pour valider l'email
				return valEmailRegex.test(email);
			},
			message: "L'email n'est pas valide",
		},
	},
	pwd: {
		type: String,
		required: true, // Le mot de passe est requis
		validate: {
			validator: function (pwd) {
				// Expression régulière pour valider l'email
				return valPWD.test(pwd);
			},
			message: "L'email n'est pas valide",
		},
	},
});
//
//
//
const userSchema = new mongoose.Schema({
	name: {
		type: String,
		unique: true, // Assure l'unicité du nom
		required: true, // Le nom est requis
		minlength: 3, // Le nom doit contenir au moins 3 caractères
		maxlength: 24, // Le nom doit contenir au plus 20 caractères
		trim: true, // Le nom ne doit pas contenir d'espaces en début ou fin
		lowercase: true, // Le nom ne doit pas contenir de majuscules
		validate: {
			validator: function (pseudo) {
				// Expression régulière pour valider le nom
				return valPseudo.test(pseudo);
			},
			message: "Le nom n'est pas valide",
		},
	},
	email: {
		type: String,
		unique: true, // Assure l'unicité de l'email
		required: true, // L'email est requis
		validate: {
			validator: function (email) {
				// Expression régulière pour valider l'email
				return valEmailRegex.test(email);
			},
			message: "L'email n'est pas valide",
		},
	},
	pwd: {
		type: String,
		required: true, // Le mot de passe est requis
		validate: {
			validator: function (pwd) {
				// Expression régulière pour valider l'email
				return valPWD.test(pwd);
			},
			message: "L'email n'est pas valide",
		},
	},
	role: {
		type: String,
		enum: ['normal', 'premium', 'admin'], // Les choix possibles pour le rôle
		default: 'normal', // Le rôle par défaut
	},
	dateInscription: {
		type: Date,
		default: Date.now,
	},
	questions: [], // Champ pour les questions de l'utilisateur
	commentaires: [], // Champ pour les commentaires de l'utilisateur
	mess: [], // Champ pour les questions de l'utilisateur
	recM: [], // Champ pour les commentaires de l'utilisateur
	// Autres champs spécifiques à votre application
});
//

// Schéma pour un commentaire
const commentSchema = new mongoose.Schema({
	content: {
		type: String,
		maxlength: 3000, // Limite de 3000 caractères pour le contenu du commentaire
	},
	author: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User', // Référence au schéma de l'utilisateur
		required: true,
	},
	createdAt: {
		type: Date,
		default: Date.now, // Date de création du commentaire
	},
	hour: {
		type: Number,
		required: true,
		default: function () {
			return this.createdAt.getHours();
		},
	},
	year: {
		type: Number,
		required: true,
		default: function () {
			return this.createdAt.getFullYear();
		},
	},
	month: {
		type: Number,
		required: true,
		default: function () {
			return this.createdAt.getMonth() + 1; // Les mois sont indexés à partir de 0, donc on ajoute 1 pour obtenir le mois réel
		},
	},
	// Autres champs de commentaire
});

//

const dbSchema = new mongoose.Schema({
	db: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 50,
	},
	collec: {
		type: String,
		required: true,
		min: 3,
		max: 100,
	},
	// Ajoutez autant de champs que nécessaire avec les contraintes appropriées
});
//
//
//
// Créer un modèle à partir du schéma
const User = mongoose.model('User', userSchema);
const Comment = mongoose.model('Comment', commentSchema);
const Db = mongoose.model('Db', dbSchema);

module.exports = {User, Comment}; // Exporter le modèle pour une utilisation dans d'autres fichiers
