const express = require('express');
// const MongoClient = require('mongodb/lib/mongo_client');
const MongoClient = require('mongodb').MongoClient;
const app = express();
const {User, Db} = require('./models/schemas');
console.log('🚀 ~ Db:', Db);
console.log('🚀 ~ User:', User);

const username = encodeURIComponent('<username>');
const password = encodeURIComponent('<password>');
const cluster = '<clusterName>';
const authSource = '<authSource>';
const authMechanism = '<authMechanism>';
// let uri = `mongodb+srv://${username}:${password}@${cluster}/?authSource=${authSource}&authMechanism=${authMechanism}`;

// const db = 'game';
// const dbMflix = 'sample_mflix';
// const dbCreate = 'eli_blog';

const uriInput = `mongodb+srv://eli:eli@clustereli.1mm4wrw.mongodb.net/?retryWrites=true&w=majority&appName=ClusterEli`;

const uri = `mongodb+srv://eli:eli@clustereli.1mm4wrw.mongodb.net/${dbCreate}?retryWrites=true&w=majority&appName=ClusterEli`;
//
console.log('🚀 ~ uri : ', uri);
// console.log('🚀 ~ uriMflix:', uriMflix);

// Create a Mongoclient with a Mongoclient Options object to set the Stable API version

const options = {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	serverApi: {
		version: '1',
		strict: true,
		deprecationErrors: true,
	},
};
const client = new MongoClient(uri, options);

const run = async (dbCreate, colec1, colec2) => {
	try {
		if (dbCreate) {
			await client.connect();
			console.log('Le client est connecté avec succès.');
		}

		if (colec1) {
			await client.db(dbCreate).createCollection(colec1);
			console.log(
				`La collection ${colec1} a été créée avec succès dans la base de données ${dbCreate}.`
			);
		}

		if (colec2) {
			await client.db(dbCreate).createCollection(colec2);
			console.log(
				`La collection ${colec2} a été créée avec succès dans la base de données "game".`
			);
		}
	} catch (error) {
		console.error('Une erreur est survenue:', error);
	} finally {
		try {
			await client.close();
			console.log('Le client est fermé avec succès.');
		} catch (error) {
			console.error(
				'Une erreur est survenue lors de la fermeture du client:',
				error
			);
		}
	}
};

run(dbCreate, colec1, colec2);

// run().catch(console.dir);
