// Affiche le bandeau d'annonce
function showAnnouncement() {
	document.getElementById('announcementBar').style.display = 'block';
	// Disparaît après 3 secondes
	setTimeout(hideAnnouncement, 3000);
}

// Cache le bandeau d'annonce
function hideAnnouncement() {
	document.getElementById('announcementBar').style.display = 'none';
}

// Affiche le bandeau d'annonce au chargement de la page
window.onload = function () {
	showAnnouncement();
};
