window.addEventListener('DOMContentLoaded', () => {
	const openPopup = () => {
		const dbV = document.getElementById('db').value;
		const collecV = document.getElementById('collec').value;

		console.log('🚀 ~ POST ~ db / collec : ', dbV, '/', collecV);
		const dbPop = document.getElementById('dbV');
		const collecPop = document.getElementById('collecV');
		dbPop.textContent = 'DataBase : ' + dbV;
		collecPop.textContent = 'Collection : ' + collecV;
	};

	const chatBubble = document.getElementById('chatBubble');
	const chatBtn = document.getElementById('chatBtn');
	const chatTog = document.getElementById('chatTog');
	chatTog.addEventListener('click', toggleChat);
	chatTog.addEventListener('click', openPopup);

	function toggleChat() {
		var chatBubble = document.getElementById('chatBubble');
		chatBubble.classList.toggle('closed');
		chatBubble.classList.toggle('open');
		openPopup();
	}
});
