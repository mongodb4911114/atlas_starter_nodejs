const mongoose = require('mongoose');

const uri = `mongodb+srv://eli:eli@clustereli.1mm4wrw.mongodb.net/?retryWrites=true&w=majority&appName=ClusterEli`;

// Connectez-vous à votre base de données MongoDB
mongoose.connect('uri', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

// Obtenez une référence à la connexion
const db = mongoose.connection;

// Vérifiez si la connexion est réussie ou s'il y a une erreur
db.on(
	'error',
	console.error.bind(console, 'Erreur de connexion à la base de données :')
);
db.once('open', function () {
	console.log('Connexion à la base de données réussie.');

	// Supprimez la base de données
	db.dropDatabase((err, result) => {
		if (err) {
			console.error(
				'Erreur lors de la suppression de la base de données :',
				err
			);
		} else {
			console.log('La base de données a été supprimée avec succès.');
		}

		// Fermez la connexion après avoir supprimé la base de données
		mongoose.connection.close();
	});
});
