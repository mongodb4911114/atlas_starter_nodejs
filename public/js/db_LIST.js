const mongoose = require('mongoose');

// Connectez-vous à votre base de données MongoDB
mongoose.connect('mongodb://localhost:27017/admin', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

// Obtenez une référence à la connexion
const db = mongoose.connection;

// Vérifiez si la connexion est réussie ou s'il y a une erreur
db.on(
	'error',
	console.error.bind(console, 'Erreur de connexion à la base de données :')
);
db.once('open', async function () {
	console.log('Connexion à la base de données réussie.');

	try {
		// Obtenez la liste des bases de données
		const adminDb = db.useDb('admin'); // Utilisation de la base de données admin
		const dbList = await adminDb.admin().listDatabases();

		console.log('Liste des bases de données MongoDB :');
		dbList.databases.forEach((db) => {
			console.log(db.name);
		});
	} catch (err) {
		console.error(
			'Erreur lors de la récupération de la liste des bases de données :',
			err
		);
	} finally {
		// Fermez la connexion après avoir récupéré la liste des bases de données
		mongoose.connection.close();
	}
});
