window.addEventListener('DOMContentLoaded', () => {
	const submit = document.getElementById('submit');
	const pop = document.getElementById('pop');
	const close = document.getElementById('close');
	pop.style.display = 'none';

	const closePop = () => {
		pop.style.display = 'none';
	};

	close.addEventListener('click', closePop);

	const openPopup = () => {
		const dbV = document.getElementById('db').value;
		const collecV = document.getElementById('collec').value;

		console.log('🚀 ~ POST ~ db / collec : ', dbV, '/', collecV);
		const dbPop = document.getElementById('dbV');
		const collecPop = document.getElementById('collecV');
		dbPop.textContent = 'DataBase : ' + dbV;
		collecPop.textContent = 'Collection : ' + collecV;
		pop.style.display = 'block';
	};

	submit.addEventListener('click', (e) => {
		e.preventDefault();
		openPopup();
	});
});
