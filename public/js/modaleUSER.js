window.addEventListener('DOMContentLoaded', () => {
	const modale = document.getElementById('popup');
	//
	const submit = document.getElementById('submit');
	//
	const closePopup = () => {
		modale.style.display = 'none';
	};

	const close = document.getElementById('close');
	close.addEventListener('click', closePopup);

	//

	//

	const user = document.getElementById('user');
	const email = document.getElementById('email');
	const pwd = document.getElementById('pwd');

	const openPopup = () => {
		// const role = document.getElementById('role').value;
		user.textContent = user.value;
		email.textContent = email.value;
		pwd.textContent = pwd.value;
		modale.style.display = 'block';
	};

	submit.addEventListener('submit', (e, user, email, pwd, role) => {
		e.preventDefault();

		console.log(
			'🚀 ~ Votre USER est : ',
			user,
			'/',
			email,
			'/',
			pwd,
			'/',
			role
		);
		openPopup();
	});
});
