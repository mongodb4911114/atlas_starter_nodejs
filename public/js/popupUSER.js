window.addEventListener('DOMContentLoaded', () => {
	const submit = document.getElementById('submit');
	const popup = document.getElementById('popup');
	const close = document.getElementById('close');

	const openPopup = () => {
		popup.style.display = 'block';
	};

	const closePopup = () => {
		popup.style.display = 'none';
	};

	close.addEventListener('click', closePopup);

	submit.addEventListener('submit', (e) => {
		e.preventDefault();

		userName = document.getElementById('name').value;
		userEmail = document.getElementById('email').value;
		userPwd = document.getElementById('pwd').value;
		userRole = document.getElementById('role').value;

		const userOne = {
			name: userName,
			email: userEmail,
			pwd: userPwd,
			role: userRole,
		};
		//
		// collec = document.getElementById('collec').value;
		console.log('🚀 ~ POST USERONE : ', userOne, '/');
		openPopup();
		// res.render('popup', {db, collec});
	});
});
