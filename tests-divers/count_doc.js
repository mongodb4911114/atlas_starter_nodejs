const countDoc = (db, collec) => {
	return client
		.connect()
		.then(() => {
			return client
				.db(db)
				.collection(collec)
				.countDocuments()
				.then((count) => {
					console.log(
						`Il y a ${count} DOCUMENTS dans la collection ${collec}.`
					);
					return count; // Retourne le nombre de documents
				})
				.catch((error) => {
					console.error(
						'ERREUR lors du COMPTE des documents :',
						error
					);
					throw error; // Propagation de l'erreur
				});
			// .finally(() => {
			// 	return client
			// 		.close()
			// 		.then(() => {
			// 			console.log('CONNEXION FERMÉE avec succès.');
			// 		})
			// 		.catch((error) => {
			// 			console.error(
			// 				`COUNTDOC -  Une erreur est survenue lors de la fermeture de la connexion - COUNTDOC`,
			// 				error
			// 			);
			// 		});
			// });
		})
		.catch((error) => {
			console.error(
				'Une erreur est survenue lors de la connexion :',
				error
			);
			throw error; // Propagation de l'erreur
		});
};
