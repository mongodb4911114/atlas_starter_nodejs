const client = new MongoClient(uri, options);

const createUSER = (oneUser, db) => {
	client
		.connect()
		.then(() => {
			console.log(`CONNECTÉ en CREATE USER à ${db} `);

			return (
				client
					.db(db)
					// .collection(collec)
					.insertOne(oneUser)
					.then(() => {
						console.log(
							`User :  ${oneUser}  créée avec succès dans la base de données ${db}.`
						);
					})
					.catch((error) => {
						console.error(
							`Erreur  lors de la création du User  :`,
							error
						);
					})
			);
		})

		.catch((error) => {
			console.error('Une erreur est survenue:', error);
		})
		.finally(() => {
			client
				.close()
				.then(() => {
					console.log('CLIENT CREATE USER fermé avec succès.');
				})
				.catch((error) => {
					console.error(
						'Erreur lors de la CLOTURE de CREATE USER ',
						error
					);
				});
		});
};
