const mongoose = require('mongoose');

//## ----------------------------------------------- URI ---------------------------------------------------------------

const uri =
	'mongodb+srv://eli:eli@clustereli.1mm4wrw.mongodb.net/membres?retryWrites=true&w=majority&appName=ClusterEli';
const match = uri.match(/@([^.]*)\./);
const clusterName = match[1].toUpperCase();

//## -------------------------------------------------------------------------------------------------------------------

// Créer une connexion à la base de données MongoDB
mongoose
	.connect(uri, {})
	.then(() => {
		console.log('Connecté à la base de données');
		console.log('Base de données connectée:', mongoose.connection.name);
	})
	.catch((error) => {
		console.error(
			'Erreur lors de la connexion à la base de données:',
			error
		);
	});

// Récupérer la connexion MongoDB
// const db = mongoose.connection;
const dbConnect = 'membres';
const colleConnect = 'normal';

// const dbInstance = mongoose.connection.useDb(dbConnect);

const dbInstance = mongoose.connection;

dbInstance.useDb(dbConnect);

console.log('MONGOOSE NAME ---> Base connectée:', mongoose.connection.name);

//## -------------------------------------------------------------------------------------------------------------------

// Définir le schéma du document
const userSchema = new mongoose.Schema({
	name: String,
	email: String,
	age: Number,
});

// Créer le modèle à partir du schéma
const User = mongoose.model('lambda', userSchema);

// Créer une nouvelle instance du modèle
const newUser = new User({
	name: 'Jojo',
	email: 'jojo@example.com',
	age: 30,
});

// Enregistrer le document dans la base de données
newUser
	.save()
	.then((savedUser) => {
		console.log('Utilisateur enregistré:', savedUser);
	})
	.catch((error) => {
		console.error(
			"Erreur lors de l'enregistrement de l'utilisateur:",
			error
		);
	});
