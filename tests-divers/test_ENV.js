require('dotenv').config(); // Charge les variables d'environnement à partir du fichier .env

//## ----------------------------------------------- URI ---------------------------------------------------------------

//
// const dbConnect = 'membres';

// const uri = `mongodb+srv://eli:eli@clustereli.1mm4wrw.mongodb.net/${dbConnect}?retryWrites=true&w=majority&appName=ClusterEli`;

const port = process.env.PORT;
const uriMEMBRES = process.env.URI_MEMBRES;
console.log('🚀 ~ uriMEMBRES:', uriMEMBRES);
